﻿using System.Collections.Generic;

namespace TestDoubles.Tests
{
    public class FakeAmazingThingDao : IAmazingThingDao
    {
        private readonly IList<AmazingThing> amazingThings;

        public FakeAmazingThingDao(IList<AmazingThing> amazingThings)
        {
            this.amazingThings = amazingThings;
        }

        public IList<AmazingThing> GetAll()
        {
            return amazingThings;
        }
    }
}