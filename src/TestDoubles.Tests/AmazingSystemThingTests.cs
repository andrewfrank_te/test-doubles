using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace TestDoubles.Tests
{
    [TestClass]
    public class AmazingSystemThingTests
    {
        private AmazingSystemThing subject;

        [TestClass]
        public class AmazingThingsHaveAStatusOf : AmazingSystemThingTests
        {
            private readonly IList<AmazingThing> amazingThings = new List<AmazingThing>
                {
                    new AmazingThing { Status = AmazingThingStatus.Incredible },
                    new AmazingThing { Status = AmazingThingStatus.Outstanding },
                    new AmazingThing { Status = AmazingThingStatus.SuperDuper }
                };

            [TestMethod]
            public void ReturnsTrueIfAnyOfTheItemsHaveAMatchingStatusUsingFakeDao()
            {
                FakeEmailer fakeEmailer = new FakeEmailer("test@example.com", "test-from@example.com", $"We found the \"Outstanding\" status!");
                subject = new AmazingSystemThing(new FakeAmazingThingDao(amazingThings), fakeEmailer);

                bool result = subject.AmazingThingsHaveAStatusOf(AmazingThingStatus.Outstanding);

                Assert.IsTrue(result);
                Assert.IsTrue(fakeEmailer.IsSendEmailCalledWithExpectedValues);
            }

            [TestMethod]
            public void ReturnsTrueIfAnyOfTheItemsHaveAMatchingStatusUsingMockDao()
            {
                // Setup the mock object
                Mock<IAmazingThingDao> mockDao = new Mock<IAmazingThingDao>();
                mockDao.Setup(m => m.GetAll()).Returns(amazingThings);

                // If we couldn't create a mock emailer, how would we verify that the SendEmail
                // method was called with the expected values?
                Mock<IEmailer> mockEmailer = new Mock<IEmailer>();

                // Pass instances of the mocks to the AmazingSystemThing
                subject = new AmazingSystemThing(mockDao.Object, mockEmailer.Object);

                // Act on the AmazingThingsHaveAStatusOf method
                bool result = subject.AmazingThingsHaveAStatusOf(AmazingThingStatus.Outstanding);

                // Verify that the GetAll method on the mock is called once and only once
                mockDao.Verify(m => m.GetAll(), Times.Once());

                // Verify that an email is sent to the appropriate people, from the appropriate people, with the
                // correct message.
                mockEmailer.Verify(m => m.SendEmail(
                    "test@example.com",
                    "test-from@example.com",
                    "We found the \"Outstanding\" status!"), Times.Once());

                Assert.IsTrue(result);
            }

            [TestMethod]
            public void ReturnsTrueIfAnyOfTheItemsHaveAMatchingStatusUsingMockDaoStubOnly()
            {
                // Sometimes, you just want to see if you get the correct result...
                Mock<IAmazingThingDao> mockDao = new Mock<IAmazingThingDao>();
                mockDao.Setup(m => m.GetAll()).Returns(amazingThings);

                Mock<IEmailer> mockEmailer = new Mock<IEmailer>();

                subject = new AmazingSystemThing(mockDao.Object, mockEmailer.Object);

                bool result = subject.AmazingThingsHaveAStatusOf(AmazingThingStatus.SuperDuper);
                Assert.IsTrue(result);
            }

            [TestMethod]
            public void ReturnsFalseIfNoMatchIsFound()
            {
                Mock<IAmazingThingDao> mockDao = new Mock<IAmazingThingDao>();
                mockDao.Setup(m => m.GetAll()).Returns(new List<AmazingThing>());

                Mock<IEmailer> mockEmailer = new Mock<IEmailer>();

                subject = new AmazingSystemThing(mockDao.Object, mockEmailer.Object);

                bool result = subject.AmazingThingsHaveAStatusOf(AmazingThingStatus.Incredible);
                Assert.IsFalse(result);
                // Also verify that the SendEmail method is never called...
                mockEmailer.Verify(m => m.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()), Times.Never());
            }
        }
    }
}
