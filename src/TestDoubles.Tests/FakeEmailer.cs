namespace TestDoubles.Tests
{
    internal class FakeEmailer : IEmailer
    {
        private readonly string expectedBody;
        private readonly string expectedFrom;
        private readonly string expectedTo;

        public bool IsSendEmailCalledWithExpectedValues { get; private set; }

        public FakeEmailer(string expectedTo, string expectedFrom, string expectedBody)
        {
            this.expectedBody = expectedBody;
            this.expectedFrom = expectedFrom;
            this.expectedTo = expectedTo;
        }

        public void SendEmail(string to, string from, string body)
        {
            // This code doesn't do anything, it just pretends to do something.
            IsSendEmailCalledWithExpectedValues =
                IsMatch(to, expectedTo) && IsMatch(from, expectedFrom) && IsMatch(body, expectedBody);
        }

        private bool IsMatch(string actual, string expected)
        {
            return actual == expected;
        }
    }
}