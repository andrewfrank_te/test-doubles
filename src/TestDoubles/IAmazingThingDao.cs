﻿using System.Collections.Generic;

namespace TestDoubles
{
    public interface IAmazingThingDao
    {
        IList<AmazingThing> GetAll();
    }
}