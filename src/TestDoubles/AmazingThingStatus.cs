namespace TestDoubles
{
    public enum AmazingThingStatus
    {
        Incredible,
        Outstanding,
        SuperDuper
    }
}