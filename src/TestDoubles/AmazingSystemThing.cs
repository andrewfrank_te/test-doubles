using System.Collections.Generic;

namespace TestDoubles
{
    public class AmazingSystemThing
    {
        private IAmazingThingDao amazingThingDao;
        private IEmailer emailer;

        public AmazingSystemThing(IAmazingThingDao amazingThingDao, IEmailer emailer)
        {
            this.amazingThingDao = amazingThingDao;
            this.emailer = emailer;
        }

        public bool AmazingThingsHaveAStatusOf(AmazingThingStatus status)
        {
            IList<AmazingThing> amazingThings = amazingThingDao.GetAll();
            foreach (AmazingThing amazingThing in amazingThings)
            {
                if (amazingThing.Status == status)
                {
                    emailer.SendEmail("test@example.com", "test-from@example.com", $"We found the \"{status}\" status!");
                    return true;
                }
            }
            return false;
        }
    }
}