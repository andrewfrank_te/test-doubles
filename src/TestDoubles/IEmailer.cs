namespace TestDoubles
{
    public interface IEmailer
    {
        void SendEmail(string to, string from, string body);
    }
}